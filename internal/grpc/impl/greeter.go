package impl

import (
	"context"
	"fmt"

	"go.uber.org/zap"

	"go-uber-fx-grpc-showcase/internal/generated"
)

func (g *Greeter) Greet(_ context.Context, request *greeting.GreetRequest) (*greeting.GreetResponse, error) {
	g.logger.Info("Processing gRPC request", zap.String("name", request.Name))

	response := &greeting.GreetResponse{
		Message: fmt.Sprintf("Hello %s from autoinjected gRPC service!", request.Name),
	}

	return response, nil
}

func NewGreeter(logger *zap.Logger) *Greeter {
	return &Greeter{logger: logger}
}

type Greeter struct {
	logger *zap.Logger
}
