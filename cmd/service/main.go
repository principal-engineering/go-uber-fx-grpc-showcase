package main

import (
	"go.uber.org/fx"
	"go.uber.org/fx/fxevent"
	"go.uber.org/zap"
	"google.golang.org/grpc"

	greeting "go-uber-fx-grpc-showcase/internal/generated"
	grpcService "go-uber-fx-grpc-showcase/internal/grpc/impl"
	grpcServer "go-uber-fx-grpc-showcase/internal/grpc/server"
)

func main() {
	fx.New(
		fx.Provide(
			zap.NewExample,

			// Annotate gRPC server instance as grpc.ServiceRegistrar
			fx.Annotate(
				grpcServer.NewGRPCServer,
				fx.As(new(grpc.ServiceRegistrar)),
			),

			// Annotate service as generated interface
			fx.Annotate(
				grpcService.NewGreeter,
				fx.As(new(greeting.GreeterServer)),
			),
		),
		fx.Invoke(
			// Start annotated gRPC server
			func(grpc.ServiceRegistrar) {},

			// Invoke service registration using annotated gRPC server and annotated service
			greeting.RegisterGreeterServer,
		),
		fx.WithLogger(func(logger *zap.Logger) fxevent.Logger {
			return &fxevent.ZapLogger{Logger: logger}
		}),
	).Run()
}
